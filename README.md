## Summary
This plugin enables up voting and down voting of comments to Jira issues. This makes Jira more social and could be seen as a first step to use for a Q&A site

## Build
1. Run `atlas-mvn package`